import { Button, IconButton } from "@material-ui/core";
import {
	AccessTime,
	Duo,
	ExpandMore,
	Inbox,
	LabelImportant,
	NearMe,
	Note,
	Person,
	Phone,
	Star,
} from "@material-ui/icons";
import AddIcon from "@material-ui/icons/Add";
import React from "react";
import { useDispatch } from "react-redux";
import { openSendMessage } from "./features/mailSlice";
import "./Sidebar.css";
import SidebarOption from "./SidebarOption";

function Sidebar() {
	const dispatch = useDispatch();

	return (
		<div className="sidebar">
			<Button
				startIcon={<AddIcon fontSize="large" />}
				className="sidebar__compose"
				onClick={() => dispatch(openSendMessage())}
			>
				Compose
			</Button>

			<SidebarOption Icon={Inbox} title="Inbox" number={54} selected={true} />
			<SidebarOption Icon={Star} title="Starred" number={23} />
			<SidebarOption Icon={AccessTime} title="Snoozed" number={46} />
			<SidebarOption Icon={LabelImportant} title="Important" number={54} />
			<SidebarOption Icon={NearMe} title="Sent" number={12} />
			<SidebarOption Icon={Note} title="Drafts" number={3} />
			<SidebarOption Icon={ExpandMore} title="More" number={43} />

			<div className="sidebar__footer">
				<div className="sidebar__footerIcons">
					<IconButton>
						<Person />
					</IconButton>
					<IconButton>
						<Duo />
					</IconButton>
					<IconButton>
						<Phone />
					</IconButton>
				</div>
			</div>
		</div>
	);
}

export default Sidebar;
