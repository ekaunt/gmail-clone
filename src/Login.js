import React from "react";
import "./Login.css";
import logo from "./img/logo.png";
import { Button } from "@material-ui/core";
import { auth, provider } from "./firebase";
import { login } from "./features/userSlice";
import { useDispatch } from "react-redux";
import { signInWithPopup } from "firebase/auth";

function Login() {
	const dispatch = useDispatch();

	const signIn = () => {
		signInWithPopup(auth, provider)
			.then(({ user }) => {
				dispatch(
					login({
						displayName: user.displayName,
						email: user.email,
						photoURL: user.photoURL,
					})
				);
			})
			.catch((error) => alert(error.message));
	};

	return (
		<div className="login">
			<div className="login__container">
				<img src={logo} alt="" />
				<Button variant="contained" color="primary" onClick={signIn}>
					Login with Google
				</Button>
			</div>
		</div>
	);
}

export default Login;
