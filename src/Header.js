import { Avatar, IconButton } from "@material-ui/core";
import {
	Apps,
	ArrowDropDown,
	Menu,
	Notifications,
	Search,
} from "@material-ui/icons";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { logout, selectUser } from "./features/userSlice";
import { auth } from "./firebase";
import { signOut } from "firebase/auth";
import "./Header.css";
import logo from "./img/logo.png";

function Header() {
	const user = useSelector(selectUser);
	const dispatch = useDispatch()

	const logOut = () => {
		signOut(auth);
		dispatch(logout());
	};

	return (
		<div className="header">
			<div className="header__left">
				<IconButton>
					<Menu />
				</IconButton>
				<img src={logo} alt="" />
			</div>

			<div className="header__middle">
				<Search />
				<input placeholder="Search mail" type="text" />
				<ArrowDropDown className="header__inputCaret" />
			</div>

			<div className="header__right">
				<IconButton>
					<Apps />
				</IconButton>
				<IconButton>
					<Notifications />
				</IconButton>
				<Avatar src={user?.photoURL} onClick={logOut} className="header__avatar" />
			</div>
		</div>
	);
}

export default Header;
