import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth, GoogleAuthProvider } from "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
	apiKey: "AIzaSyDsbPNSt0KNKTUPbTw5KJGnraBgrhr8aJY",
	authDomain: "clone-bb643.firebaseapp.com",
	projectId: "clone-bb643",
	storageBucket: "clone-bb643.appspot.com",
	messagingSenderId: "245095355372",
	appId: "1:245095355372:web:f15eba8c1d532291e126c9",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth(app);
const provider = new GoogleAuthProvider();

export { db, auth, provider };
